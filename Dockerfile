FROM php:7.4-alpine
WORKDIR /var/www/html
COPY . /var/www/html
RUN apk --update --no-cache add git
RUN docker-php-ext-install pdo_mysql
COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN composer install
CMD [ "php", "/var/www/html/index.php" ]