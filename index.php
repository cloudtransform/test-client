<?php

require_once __DIR__ . '/vendor/autoload.php';

if (isset($argc) && isset($argv) && isset($argv[0]) && isset($argv[1])) {
	$counter = $argv[1];
	for ($i = 0; $i < $counter; $i++) {
		sendQuestions($i);
	}
}
else {
	$counter = 0;
	while(true) {
		sendQuestions($counter);
		$counter += 1;
	}
	
}

	function sendQuestions($index)
    {
    	$client = new \GuzzleHttp\Client(["base_uri" => "http://node-server:8080"]);
    	try {
		    
	        $options = [
	            'json' => [
	                "questionId" => $index
	            ]
	        ];
	        $response = $client->post("/api/question", $options);

	        $result = json_decode($response->getBody());

	        $answerId = $result->{'answerId'};
	        $response = array('answerId' => $answerId);
	        echo json_encode($response);
	        echo "\r\n";

	        if ($answerId != $index) {
	        	echo "error";
	        }
			
		} catch (\GuzzleHttp\Exception\GuzzleException $e) {
		    echo $e->getMessage();
	        echo "\r\n";
		}
    }

?>
