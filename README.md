# CLIENT test app
This client is build to test the commmunication between pods when and the update of a cluster.
This app will continuously send messages to a server. This only works if the server is online.

After the server is installed and running on a kubernetes cluster, the application can be run.

## Creating the image
Run the following command:

````shell script
docker build . -t client
````
This will create an image called client.


## Running the client on a kubernetes cluster
Run the following command:

````shell script
kubectl apply -f deployment.yaml
````
This command will pull the geert263/frontend image from docker hub.
You can change this in the deployment.yaml to your own image.